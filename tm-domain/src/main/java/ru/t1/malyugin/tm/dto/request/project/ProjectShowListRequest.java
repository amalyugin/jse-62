package ru.t1.malyugin.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.enumerated.EntitySort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowListRequest extends AbstractUserRequest {

    @Nullable
    private EntitySort sort;

    public ProjectShowListRequest(
            @Nullable final String token,
            @Nullable final EntitySort sort
    ) {
        super(token);
        this.sort = sort;
    }

}