package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Show task by id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK BY ID]");

        System.out.print("ENTER TASK ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken(), id);
        @Nullable final TaskDTO task = taskEndpoint.showTaskById(request).getTask();
        renderTask(task);
    }

}