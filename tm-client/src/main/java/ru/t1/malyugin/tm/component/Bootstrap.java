package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.service.ILoggerService;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.malyugin.tm.listener.AbstractListener;
import ru.t1.malyugin.tm.util.SystemUtil;
import ru.t1.malyugin.tm.util.TerminalUtil;

import javax.annotation.PostConstruct;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]\n");
                loggerService.command(command);
            } catch (@NotNull final SOAPFaultException soapFaultException) {
                loggerService.errorSOAP(soapFaultException);
                System.out.println("[FAIL]\n");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]\n");
            }
        }
    }

    public void processCommand(@Nullable final String command) {
        if (StringUtils.isBlank(command)) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(@Nullable final String argument) {
        if (StringUtils.isBlank(argument)) return;
        @Nullable final AbstractListener abstractListener = getListenerByArgument(argument);
        if (abstractListener == null) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(abstractListener);
    }

    private AbstractListener getListenerByArgument(@NotNull final String arg) {
        return Arrays.stream(listeners).filter(l -> arg.equals(l.getArgument())).findAny().orElse(null);
    }

    @PostConstruct
    private void prepareStartup() {
        BasicConfigurator.configure();
        initPID();
        loggerService.info("** WELCOME TO TM CLIENT **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::preExitClient));
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

    public void preExitClient() {
        loggerService.info("** TM CLIENT IS SHUTTING DOWN **");
    }

}