package ru.t1.malyugin.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getQueueName();

    @NotNull
    String getQueueUrl();

    @NotNull
    String getMongoHost();

    @NotNull
    String getMongoPort();

    @NotNull
    String getMongoDBName();

}