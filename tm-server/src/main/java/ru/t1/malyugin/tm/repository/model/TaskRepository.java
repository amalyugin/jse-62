package ru.t1.malyugin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractWBSRepository<Task> {

    @NotNull
    List<Task> findAllByUserAndProjectId(@NotNull User user, @NotNull String projectId);

    @Modifying
    @Query("UPDATE Task t SET t.project = :project WHERE t.id = :taskId")
    void setProjectById(@NotNull @Param("taskId") String taskId,
                        @Nullable @Param("project") Project project);

}