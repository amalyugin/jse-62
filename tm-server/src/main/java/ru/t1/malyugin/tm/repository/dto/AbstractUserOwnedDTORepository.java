package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.malyugin.tm.dto.model.AbstractUserOwnedDTOModel;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel> extends AbstractDTORepository<M> {

    List<M> findAllByUserId(@NotNull String userId);

    Optional<M> findByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}