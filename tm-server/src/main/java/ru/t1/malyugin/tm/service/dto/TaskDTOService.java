package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.api.service.dto.ITaskDTOService;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.repository.dto.TaskDTORepository;

import java.util.Collections;
import java.util.List;

@Service
public class TaskDTOService extends AbstractWBSDTOService<TaskDTO> implements ITaskDTOService {

    @NotNull
    private final IProjectDTOService projectDTOService;

    @NotNull
    private final TaskDTORepository taskDTORepository;

    @Autowired
    public TaskDTOService(
            @NotNull final IProjectDTOService projectDTOService,
            @NotNull final TaskDTORepository taskDTORepository
    ) {
        this.projectDTOService = projectDTOService;
        this.taskDTORepository = taskDTORepository;
    }

    @NotNull
    @Override
    protected TaskDTORepository getRepository() {
        return taskDTORepository;
    }

    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(userId);
        taskDTO.setName(name);
        if (!StringUtils.isBlank(description)) taskDTO.setDescription(description);
        add(taskDTO);
        return taskDTO;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final TaskDTO taskDTO = findOneById(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setName(name);
        if (!StringUtils.isBlank(description)) taskDTO.setDescription(description);
        update(taskDTO);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        return getRepository().findAllByUserIdAndProjectId(userId.trim(), projectId.trim());
    }

    @Override
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (!projectDTOService.existsById(userId.trim(), projectId.trim())) throw new ProjectNotFoundException();
        getRepository().setProjectById(taskId.trim(), projectId.trim());
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (!projectDTOService.existsById(userId.trim(), projectId.trim())) throw new ProjectNotFoundException();
        getRepository().setProjectById(taskId.trim(), null);
    }

}