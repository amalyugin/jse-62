package ru.t1.malyugin.tm.api.component;

import org.jetbrains.annotations.NotNull;

public interface IJmsComponent {

    void sendMessage(@NotNull Object object, @NotNull String type);

    void stop();

}
