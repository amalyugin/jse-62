package ru.t1.malyugin.tm.service.dto;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.malyugin.tm.dto.model.AbstractUserOwnedDTOModel;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.repository.dto.AbstractUserOwnedDTORepository;

import java.util.List;

public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedDTOModel> extends AbstractDTOService<M>
        implements IUserOwnedDTOService<M> {

    @NotNull
    @Override
    protected abstract AbstractUserOwnedDTORepository<M> getRepository();

    @Override
    public long getSize(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId.trim());
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId.trim());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId.trim(), id.trim()).orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId.trim(), id.trim());
    }

    @Override
    public @NotNull List<M> findAll(@Nullable final String userId) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        return getRepository().findAllByUserId(userId.trim());
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (!getRepository().existsByUserIdAndId(userId.trim(), model.getId())) return;
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (!getRepository().existsByUserIdAndId(userId.trim(), id.trim())) return;
        getRepository().deleteByUserIdAndId(userId.trim(), id.trim());
    }

}